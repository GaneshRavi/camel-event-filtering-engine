Event Filtering Engine implementation Using Apache Camel
============================================

To run the project, execute the following command from the root of the project directory

    mvn compile camel:run

Import this project in Eclipse by clicking File -> Import -> Maven -> Existing Maven Projects
and choosing the root directory of the project

The CI used for this project is Shippable. The Tests report and Code-coverage reports can be visualised from the project's build page in Shippable.
The build artifact can be downloaded from Shippable at:
https://app.shippable.com/projects/5477cff7d46935d5fbbed3c1
