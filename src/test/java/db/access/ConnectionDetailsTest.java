package db.access;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConnectionDetailsTest {

	@Test
	public void connectionDetailConstructorTest() {
		String dbName = "testdb";
		String dbHost = "testhost";
		String dbPort = "78910";
		String dbUsername = "testusername";
		String dbPassword = "testpassword";
		ConnectionDetails connDetail = new ConnectionDetails(dbName, dbHost, dbPort, dbUsername, dbPassword);
		assertEquals("testdb", connDetail.getDbName());
		assertEquals("testhost", connDetail.getDbHost());
		assertEquals(78910, connDetail.getDbPort());
		assertEquals("testusername", connDetail.getDbUsername());
		assertEquals("testpassword", connDetail.getDbPassword());
	}

	@Test
	public void connectionDetailDefaultConstructorTest() {
		
		// Reads the connection parameters from the statewise.properties file
		ConnectionDetails connDetail = new ConnectionDetails();
		assertEquals("statewisetest", connDetail.getDbName());
		assertEquals("localhost", connDetail.getDbHost());
		assertTrue(connDetail.getDbPort() > 0);
		assertEquals("statewise", connDetail.getDbUsername());
		assertEquals("statewise", connDetail.getDbPassword());
	}
	
}
