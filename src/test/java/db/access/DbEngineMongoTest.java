package db.access;

import static org.junit.Assert.*;

import org.junit.Test;

import com.objects.Event;

public class DbEngineMongoTest {
	
	private static final DBEngineIF DB_INSTANCE = new DBEngine().getInstance();
	
	@Test
	public void testAddEvent() {
		Event event = new Event();
		event.setField("specificProblems", "test _spec_problem");
		assertTrue(DB_INSTANCE.addEvent(event) > 0);
	}
	
	@Test
	public void testClearAllEvents() {
		Event event = new Event();
		event.setField("specificProblems", "test _spec_problem");
		DB_INSTANCE.addEvent(event);
		DB_INSTANCE.clearAllEvents();
		assertEquals(1, DB_INSTANCE.addEvent(event));
	}

}
