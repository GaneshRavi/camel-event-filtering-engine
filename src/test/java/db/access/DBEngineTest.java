package db.access;

import static org.junit.Assert.*;

import org.junit.Test;

public class DBEngineTest {

	@Test
	public void getInstanceTest() {
		// An instance is created using the connection parameters in statewise.properties file
		DBEngineIF dbEngineImpl = new DBEngine().getInstance();
		assertNotNull(dbEngineImpl);
	}

}
