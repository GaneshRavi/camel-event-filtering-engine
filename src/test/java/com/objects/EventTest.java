package com.objects;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

public class EventTest {

	@Test
	public void getFieldTest() {
		Event event = new Event();
		event.setField("severity", "Critical");
		event.setField("status", "failed");
		assertEquals("Critical", event.getField("severity"));
		assertEquals("failed", event.getField("status"));
	}
	
	@Test
	public void getFieldsTest() {
		Event event = new Event();
		event.setField("severity", "Critical");
		event.setField("status", "failed");
		Map<String, Object> eventMap = event.getFields();
		assertNotNull(eventMap);
		assertEquals("Critical", eventMap.get("severity"));
		assertEquals("failed", eventMap.get("status"));
	}
	
	@Test
	public void clearFieldsTest() {
		Event event = new Event();
		event.setField("severity", "Critical");
		event.setField("status", "failed");
		event.clearFields();
		assertEquals("", event.getField("severity"));
		assertEquals("", event.getField("status"));
	}
	
	@Test
	public void toStringTest() {
		Event event = new Event();
		event.setField("severity", "Critical");
		event.setField("status", "failed");
		assertTrue(event.toString().contains("Critical"));
		assertTrue(event.toString().contains("failed"));
	}

}
