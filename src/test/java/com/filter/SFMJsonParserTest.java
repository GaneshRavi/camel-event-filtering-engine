package com.filter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.objects.Event;

public class SFMJsonParserTest {
	
	private static final String TEST_FILTER_JSON_FILE = "src/test/resources/filters/test_filter.json";
	
	private static final String TEST_FILTER_JSON_DIRECTORY = "src/test/resources/filters";
	
	private static final String MOINSTANCE = "test_moinstance-";

	private static final String ADDITIONAL_TEXT = "test add text";

	private static final String PROBABLE_CAUSE = "test_probable_cause";

	private static final String SPECIFIC_CAUSE_CORRECT = "ran_test_idu";

	private static final String SEVERITY_CLEARED = "cleared";

	private static final String SEVERITY_CRITICAL = "critical";

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	private static Logger logger = LoggerFactory.getLogger(SFMJsonParserTest.class);

	@Test
	public void parserTest() throws FileNotFoundException, IOException {
		List<FiltMapTree> fmTrees = constructFiltMapTree();
		filteringTest(fmTrees);
	}

	private static void filteringTest(List<FiltMapTree> fmTrees) {
		FilteringEngine fmEngine = new FilteringEngine(TEST_FILTER_JSON_DIRECTORY);
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < 100000; i++) {
			Event event = buildEvent(i);
			boolean hasPassedFilters = fmEngine.applyFilters(event);
			org.junit.Assert.assertTrue("failure - Event did not pass the filters", hasPassedFilters);
		}
		long finishTime = System.currentTimeMillis();
		logger.info("Total time taken to apply to filter the events (ms) : " + String.valueOf(finishTime - startTime));
	}

	private static Event buildEvent(int i) {
		Event event = new Event();
		event.setField("eventId", String.valueOf(i));
		if(i % 2 == 0) {
			event.setField("severity", SEVERITY_CRITICAL);
		} else {
			event.setField("cleared", SEVERITY_CLEARED);
		}
		event.setField("specificProblems", SPECIFIC_CAUSE_CORRECT);
		event.setField("probableCause", PROBABLE_CAUSE);
		event.setField("additionalText", ADDITIONAL_TEXT);
		event.setField("moInstance", MOINSTANCE + i);
		return event;
	}

	private static List<FiltMapTree> constructFiltMapTree() throws IOException,
			FileNotFoundException, JsonParseException {
		String fileName = TEST_FILTER_JSON_FILE;
		String sfmJson = IOUtils.toString(new FileReader(fileName));
		List<String> sfmJsons = new ArrayList<String>();
		sfmJsons.add(sfmJson);
		List<FiltMapTree> fmTrees = SFMJsonParser.read(sfmJsons);
		logger.info("Constructed SFM Tree: " + fmTrees);
		return fmTrees;
	}
	
}
