package com.filter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.filter.ConditionType;
import com.filter.FilterCondition;
import com.objects.Event;

public class FilterConditionTest {
	
	private FilterCondition equalsCond;
	private FilterCondition notEqualsCond;
	
	@Before
	public void setUp() {
		equalsCond = new FilterCondition("specificProblems", ConditionType.EQUALS, "test_spec_cause");
		notEqualsCond = new FilterCondition("specificProblems", ConditionType.NOT_EQUALS, "wrong_test_spec_cause");
	}
	
	@Test
	public void equalityPassTest() {
		Event event = new Event();
		event.setField("specificProblems", "test_spec_cause");
		assertTrue(FilterCondition.doesEventPassCondition(equalsCond, event));
	}
	
	@Test
	public void equalityFailTest() {
		Event event = new Event();
		event.setField("specificProblems", "test_spec_cause1");
		assertFalse(FilterCondition.doesEventPassCondition(equalsCond, event));
	}
	
	@Test
	public void nonEqualityPassTest() {
		Event event = new Event();
		event.setField("specificProblems", "test_spec_cause");
		assertTrue(FilterCondition.doesEventPassCondition(notEqualsCond, event));
	}
	
	@Test
	public void nonEqualityFailTest() {
		Event event = new Event();
		event.setField("specificProblems", "wrong_test_spec_cause");
		assertFalse(FilterCondition.doesEventPassCondition(notEqualsCond, event));
	}

}
