package com.filter.integration.test;

import java.io.FileReader;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventSenderIT {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private static Logger logger = LoggerFactory.getLogger(EventSenderIT.class);

	private static final String testJsonEvent = "{ \"eventRank\" : \"original\"," +
			"\"severity\": \"Critical\"," +
			"\"specificProblems\" : \"ran_test_idu\" }";

	@Test
	public void sendSingleJsonEventToRestIFTest() {
		try {
			String host = "localhost";
			String restPort = "6667";

			CamelContext camelContext = new DefaultCamelContext();
			ProducerTemplate producer = camelContext.createProducerTemplate();
			producer.sendBody("jetty:http://" + host + ":" + restPort + "/event", testJsonEvent);
			logger.info("Rest events sent.");
		} catch(Exception e) {
			logger.error("Error while sending events", e);
		}
	}

	@Test
	public void sendSingleJsonEventToSocketIFTest() {
		try {
			String host = "localhost";
			String port = "6666";

			CamelContext camelContext = new DefaultCamelContext();
			ProducerTemplate producer = camelContext.createProducerTemplate();
			producer.sendBody("mina:tcp://" + host + ":" + port + "?sync=false", testJsonEvent);
			logger.info("TCP events sent.");
		} catch(Exception e) {
			logger.error("Error while sending events", e);
		}
	}
	
	@Test
	public void sendSingleJsonFileTest() throws Exception {
		try {
			String fileName = "src/test/resources/events.json";
			String host = "localhost";
			String port = "6666";
			String textToSend;
			logger.info("Sending tcp message {} to host={}, port={}", new Object[]{ fileName, host, port});
			textToSend = IOUtils.toString(new FileReader(fileName));
			logger.debug("File size={}", textToSend.length());
			CamelContext camelContext = new DefaultCamelContext();
			ProducerTemplate producer = camelContext.createProducerTemplate();
			producer.sendBody("mina:tcp://" + host + ":" + port + "?sync=false", textToSend);
			logger.info("Events in file {} sent.", fileName);
		} catch(Exception e) {
			logger.error("Error", e);
		}
	}
}