package com.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * This is a static helper class used to access the properties in the
 * StateWise.properties file. All properties managed by this class are accessed
 * via the Java Properties class, the location of the actual properties files
 * being specified in the ENV_APPLICATION_HOME/properties
 * directory, where ENV_APPLICATION_HOME is an environment variable.
 */
public final class EventFilterPropertiesHelper {

	/** The Properties object used for getting property values. */
	private static Properties properties = new Properties();

	/**
	 * Static initializer to set the properties object from which property
	 * values may be read. The properties object will be left as null if the
	 * ENV_APPLICATION_HOME environment variable doesn't exist or is empty, or if
	 * there was an I/O error or the file was not found.
	 */
	static {
		String filename = "src/main/resources/eventfilter.properties";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filename); 
			properties.load(fis);
		} catch (FileNotFoundException e) {
			properties = null;
		} catch (IOException e) {
			properties = null;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// ignore exception
				}
			}
		}
	}

	/**
	 * Hide the constructor.
	 * @throws InstantiationException 
	 */
	private EventFilterPropertiesHelper() throws InstantiationException {
		throw new InstantiationException("Constants class should not be instantiated");
	}

	/**
	 * Get a trimmed property value from the StateWise property file.
	 * 
	 * @param key
	 *            The key associated with the value to be returned.
	 * @return The trimmed value associated with the key, or null if there was
	 *         an error getting the key or accessing the properties file.
	 */
	public static String getProperty(final String key) {
		String value = null;

		if (properties != null) {
			value = properties.getProperty(key);

			if (value != null) {
				value = value.trim();
			}
		}

		return value;
	}

}

