package com.configuration;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PropertiesConstants {
	
	/**
	 * Name of the default Name space, i.e. none value pack name space
	 */
	public static final String DEFAULT_NAMESPACE = "DefaultNamespace";
	
	/** the APPLICATION_HOME environment variable */
	public static String ENV_APPLICATION_HOME = "STATEWISE_HOME";
	
	/**
	 * The name of the StateWise properties file, as located in a subdirectory
	 * under the direectory specified by the APPLICATION_HOME environment variable.
	 */
	public static final String STATEWISE_PROPERTYFILE = "statewise.properties";
	
	/**
	 * The name of the subdirectory under APPLICATION_HOME in which the StateWise
	 * property file is kept.
	 */
	public static final String STATEWISE_PROPERTYSUBDIR = "properties";

	/**
	 * The name of the subdirectory under APPLICATION_HOME in which the jar files
	 * are placed.
	 */
	public static final String STATEWISE_JARS_DIR = "jars";
	
	/**
	 * The name of the subdirectory under STATEWISE_JARS_DIR in which the config files
	 * are placed.
	 */
	public static final String STATEWISE_JARS_CONFIGURATION_DIR = "configuration";

	/** The name of the action dialog key properties file, as located in a subdirectory
	 * under the directory specified by the APPLICATION_HOME environment variable. */
	public static final String ACTIONDLGKEY_PROPERTYFILE = "actiondialogkey.properties";
	
	/** The name of the filter field properties file, as located in a subdirectory
	 * under the directory specified by the APPLICATION_HOME environment variable. */
	public static final String EVENTFIELD_PROPERTYFILE = "filterfield.properties";

	/** The name of the filter condition key properties file, as located in a subdirectory
	 * under the directory specified by the APPLICATION_HOME environment variable. */
	public static final String CONDITIONKEY_PROPERTYFILE = "filterconditionkey.properties";

	/** The name of the filter value properties file, as located in a subdirectory
	 * under the directory specified by the APPLICATION_HOME environment variable. */
	public static final String VALUEKEY_PROPERTYFILE = "filtervaluekey.properties";
	
	/** The name of the action system functions properties file, as located in a subdirectory
	 * under the directory specified by the APPLICATION_HOME environment variable. */
	public static final String SYSTEMFUNCTIONS_PROPERTYFILE = "systemfunctions.properties";
	
	/** History properties file */
	public static final String HISTORY_PROPERTYFILE = "history.properties";
	
	/**
	 * The package location used for accessing the StateWise server configuration properties files.
	 */
	public static final String STATEWISE_SERVERCONFIGPACKAGE = "com.sidonis.statewise.server.configuration";

	/**
	 * The package location used for accessing the StateWise configuration properties files common to both client and server.
	 */
	public static final String STATEWISE_CONFIGPACKAGE = "com.sidonis.statewise.configuration";
	
	/**
	 * The package location used for accessing the Regex UI configuration properties files.
	 */
	public static final String STATEWISE_REGEXCONFIGPACKAGE = "com.sidonis.statewise.client.regexui.configuration";

	/**
	 * The package location used for accessing all the StateWise I18N Resource
	 * Bundle values (ie. i18n.properties).
	 */
	public static final String I18N_PROPERTIES = STATEWISE_CONFIGPACKAGE + ".i18n";

	/** The full package name of the ruleobject.properties bundle. */
	public static final String RULEOBJECT_RESOURCE = STATEWISE_CONFIGPACKAGE + ".ruleobject";

	/** The full package name of the ruleexistence.properties bundle. */
	public static final String RULEEXISTENCE_RESOURCE = STATEWISE_CONFIGPACKAGE + ".ruleexistence";

	/** The full package name of the rulefield.properties bundle. */
	public static final String RULEFIELD_RESOURCE = STATEWISE_CONFIGPACKAGE + ".rulefield";

	/** The full package name of the ruleconditionkey.properties bundle. */
	public static final String RULECONDITIONKEY_RESOURCE = STATEWISE_CONFIGPACKAGE + ".ruleconditionkey";
	
	/** The full package name of the key value pair kvpruleconditionkey.properties bundle. */
	public static final String KVP_RULECONDITIONKEY_RESOURCE = STATEWISE_CONFIGPACKAGE + ".kvpruleconditionkey";
	
	/** The full package name of the key value pair kvpvaluekey.properties bundle. */
	public static final String KVP_VALUEKEY_RESOURCE = STATEWISE_CONFIGPACKAGE + ".kvpvaluekey";

	/** The full package name of the key value pair kvpfield.properties bundle. */
	public static final String KVP_FIELD_RESOURCE = STATEWISE_CONFIGPACKAGE + ".kvpfield";

	/** The full package name of the rulevaluekey.properties bundle. */
	public static final String RULEVALUEKEY_RESOURCE = STATEWISE_CONFIGPACKAGE + ".rulevaluekey";
	
	/** The full package name of the rulevalueconstants.properties bundle. */
	public static final String RULEVALUECONSTANTS_RESOURCE = STATEWISE_CONFIGPACKAGE + ".rulevalueconstants";

	/** The full package name of the mappingfixedclass.properties bundle. */
	public static final String MAPPINGFIXEDCLASS_RESOURCE = STATEWISE_SERVERCONFIGPACKAGE + ".mappingfixedclass";
	
	/** The full package name of the regexcount.properties bundle. */
	public static final String REGEXCOUNT_RESOURCE = STATEWISE_REGEXCONFIGPACKAGE + ".regexcount";

	/** The full package name of the regexitem.properties bundle. */
	public static final String REGEXITEM_RESOURCE = STATEWISE_REGEXCONFIGPACKAGE + ".regexitem";
	
	/** The full package name of the objectmapping.properties bundle. */
	public static final String OBJECTMAPPING_RESOURCE = STATEWISE_CONFIGPACKAGE + ".objectmapping";

	/** The full package name of the objectmappingfield.properties bundle. */
	public static final String OBJECTMAPPINGFIELD_RESOURCE = STATEWISE_CONFIGPACKAGE + ".objectmappingfield";
	
	/**
	 * The name of the 'is stored in' condition key attribute used in the
	 * ruleconditionkey.properties bundle. This key is handled as a special case
	 * in the rules Translator code.
	 */
	public static final String CONDITION_ISSTOREDIN = "stringIsStoredIn";

	/** 
	 * Use for converting old style booleans in I18N
	 */
	public static final Object CONDITIONKEY_BOOLEAN = "conditionkey.boolean";

	
	/**
	 * The name of the trigger action property used in the actiontype.properties
	 * bundle.
	 */
	public static final String TRIGGER_PROPERTY = "triggerAction";

	/**
	 * The name of the teardown action property used in the
	 * actiontype.properties bundle.
	 */
	public static final String TEARDOWN_PROPERTY = "teardownAction";
	
	/**
	 * The name of the 'mesh object state' property as defined in the
	 * rulevaluekey.properties bundle.
	 */
	public static final String MESHOBJECT_STATES = "valuekey.meshobjstate";

	/**
	 * The name of the 'matches' property as defined in the
	 * filterconditionkey.properties bundle.
	 */
	public static final String MATCHES_PROPERTY = "matches";

	/** The properties bundle files delimit their fields with this. */
	public static final String PROPERTYITEMS_DELIMITER = ",";

	/**
	 * The properties bundle files define fields using these tokens.
	 */
	/** Defines a string type field. */
	public static final String PROPERTIESTYPE_STRING = "String";

	/** Defines a date type field. */
	public static final String PROPERTIESTYPE_DATE = "Date";

	/** Defines an integer type field. */
	public static final String PROPERTIESTYPE_INT = "Int";
	
	/** Defines an boolean type field. */
	public static final String PROPERTIESTYPE_BOOLEAN = "Boolean";
	
	/** Defines a a float type field. */
	public static final String PROPERTIESTYPE_FLOAT = "Float";
	
	/** Defines a double type field.*/
	public static final String PROPERTIESTYPE_DOUBLE = "Double";
	
	/** Defines a long type field.*/
	public static final String PROPERTIESTYPE_LONG = "Long";
	
	/** String value used in key value pair to represent false value */
	public static final String PROPERTIESBOOL_FALSE = "Boolean.false";
	
	/** String value used in key value pair to represent true value */
	public static final String PROPERTIESBOOL_TRUE = "Boolean.true";
	/**
	 * The string used in the rulevaluekey.properties and
	 * filtervaluekey.properties files to indicate that the list of classes as
	 * defined in the metamodel file should be used rather than a list of comma
	 * separated items associated with a key in the rulevaluekey.properties or
	 * filtervaluekey.properties files.
	 */
	public static final String CLASSLIST = "<CLASSLIST>";
	
	/** Signifies return of just the classes in the metamodel file. */
	public static final int CLASSLIST_BASIC = 0;

	/**
	 * Signifies return of all classes, including thos from the
	 * mappingfixedclass.properties bundle.
	 */
	public static final int CLASSLIST_FULL = 1;
	
	/**
	 * Signifies return of all class name / parent class name pairs.
	 */
	public static final int CLASSLIST_WITHPARENTS = 2;
	
	/**
	 * The names of the StateWise web services as listed by the Tomcat request.
	 * They are in order of start-up - this should not be changed
	 */
	public static final List<String> STATEWISE_WEBAPPS = Collections.unmodifiableList((Arrays.asList("FiredRulesUIServer", "MeshUIServer", "NotificationUIServer",
			"NotificationManager", "EventManager", "DataCollector", "RulesServer", "DataManager")));

	/**
	 * The names of the StateWise web services as listed by the Tomcat request.
	 * This is the shutdown order - this should not be changed
	 */
	public static final List<String> STATEWISE_WEBAPPS_SHUTDOWN = Collections.unmodifiableList((Arrays.asList( "FiredRulesUIServer", "MeshUIServer", "NotificationUIServer",
			"NotificationManager", "EventManager", "RulesServer", "DataCollector", "DataManager")));
	
	
	/** the user prefix for custom fields in filterfield.properties */
	public static final String USER_PREFIX = "user.";
	
	/** the sub-type suffix delimiter e.g. Data Object (statistics) is data:statistics
	 *  cannot use the underscore char due to constants of the form K_CONSTANT_NAME */
	public static final char SUBTYPE_SUFFIX_DELIMITER = ':';
	
	/**
	 * The following constants define the RMI services. 'Internal' services are
	 * those always provided by RMI, regardless of configuration. 'External'
	 * services are those provided by RMI as an optional alternative to the Web
	 * service versions.
	 */
	/**
	 * 'Internal' Logging RMI service ID. these IDs must be contiguous starting
	 * at 0.
	 */
	public static final int K_LOGGING_RMI_SERVICE = 0;

	/**
	 * 'Internal' Topology RMI service ID.
	 */
	public static final int K_TOPOLOGY_RMI_SERVICE = 1;
	
	/**
	 * The string used to identify the 'Mesh Update' action in
	 * get/putInterval().
	 */
	public static final String MAINTENANCE_UPDATE = "UPDATE";

	/**
	 * The string used to identify the 'Archive Update' action in
	 * get/putInterval().
	 */
	public static final String MAINTENANCE_ARCHIVE = "ARCHIVE";
	
	/** Primary 'round-trip' notification type text. */
	public static final String NOTIFTYPETEXT_PRIMARY = "PRIMARY";
	
	/** Marker 'round-trip' notification type text. */
	public static final String  NOTIFTYPETEXT_MARKER = "MARKER";
	
	/** Problem report 'round-trip' notification type text. */
	public static final String  NOTIFTYPETEXT_PROBLEMREPORT = "PROBLEMREPORT";
	
	/** Service impact 'round-trip' notification type text. */
	public static final String  NOTIFTYPETEXT_SERVICEIMPACT = "SERVICEIMPACT";
	
	/** Root cause 'round-trip' notification type text. */
	public static final String  NOTIFTYPETEXT_ROOTCAUSE = "ROOTCAUSE";

	/** command line option to tell stopper jar to stop its local GC */
	public static final String KILL_GC = "-killGC";

	/** The Key value pair property delimiter */
	public static final String PROPERTY_DELIMITER = "&";
	

	/** the directory name for the value packs */
	public static final String VALUEPACKS_DIR_NAME = "valuepacks";

	/** the notification base class */
	public static final String NOTIFICATION_BASE_CLASS = "Notification";

}
