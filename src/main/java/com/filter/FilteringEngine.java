package com.filter;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.objects.Event;

/**
 * The class which performs the filtering and mapping on
 * incoming events
 * 
 * @author Ganesh Ravi
 *
 */
public class FilteringEngine {

	private static Logger logger = LoggerFactory.getLogger(FilteringEngine.class);

	private List<FiltMapTree> filterTrees;

	public FilteringEngine(String filtersDirPath) {
		logger.info("Initialising filter tree with filters in directory: " + filtersDirPath);
		List<String> sfmJson = FilterFileReaderHelper.getAllSFMJsonFromDirectory(filtersDirPath);
		try {
			this.filterTrees = SFMJsonParser.read(sfmJson);
		} catch (JsonParseException e) {
			logger.error("Error while parsing the JSON text",e);
		} catch (IOException e) {
			logger.error("I/O Error while reading the JSON text",e);
		}
	}
	
	/**
	 * Apply all the stored filters on the given event
	 * 
	 * @param event The event which has to be filtered and mapped
	 * @return The event if the event passed the filters.
	 * null otherwise
	 */
	public boolean applyFilters(Event event) {
		if(filterTrees == null) {
			logger.error("Filter tree is not initialised. Cannot perform filtering");
			return false;
		}
		if(event == null) {
			logger.error("Event is null. Cannot perform filtering on a null event");
			return false;
		}
		Event filteredEvent = null;
		for(FiltMapTree fTree : filterTrees) {
			Scenario rootScenario = fTree.getRootScenario();
			for(Filter filter : rootScenario.getFilterChildren()) {
				filteredEvent = appplyFilterOnEvent(filter, event);
				if(filteredEvent != null) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Apples the given filter on the given event
	 * 
	 * @param currentFilter The filter which needs to be applied
	 * @param event The event on which the filters are to be applied
	 * @return The event if the event passes the filter,
	 * null otherwise
	 */
	private static Event appplyFilterOnEvent(Filter currentFilter, Event event) {
		if(currentFilter.getFilterChildren() == null) {
			if(doesEventPassConditions(currentFilter.getConditions(), event)) {
				return event;
			}
		} else if(currentFilter.getFilterChildren() != null) {
			if(doesEventPassConditions(currentFilter.getConditions(), event)) {
				for(Filter nextFilters : currentFilter.getFilterChildren()) {
					Event filteredEvent = appplyFilterOnEvent(nextFilters, event);
					if(filteredEvent != null) {
						return filteredEvent;
					}
				}
			}
		}
		return null;
	}

	/**
	 * A helper method to check if the given event passes all given
	 * conditions
	 * 
	 * @param conditions
	 * @param event
	 * @return True if the event passes all the conditions.
	 * false otherwise
	 */
	private static boolean doesEventPassConditions(
			List<FilterCondition> conditions, Event event) {
		for (FilterCondition condition : conditions) {
			if(!FilterCondition.doesEventPassCondition(condition, event)) {
				return false;
			}
		}
		return true;
	}

}