package com.filter;

import com.objects.Event;

/**
 * This class represents a Condition in a Filter
 * 
 * @author Ganesh Ravi
 *
 */
public class FilterCondition {
	
	private final String field;
    private final ConditionType condition;
    private final String value;
	
	public FilterCondition(String field, ConditionType condition, String value) {
		this.field = field;
		this.condition = condition;
		this.value = value;
	}
	
    public String getField() {
		return field;
	}
	public ConditionType getCondition() {
		return condition;
	}
	public String getValue() {
		return value;
	}
	
	/**
	 * A helper method to check if the given event passed the
	 * given condition
	 * 
	 * @param condition
	 * @param event
	 * @return True if the event passes the condition.
	 * false otherwise
	 */
	public static boolean doesEventPassCondition(FilterCondition condition,
			Event event) {
		String fieldValue = String.valueOf(event.getField(condition.getField()));
		switch(condition.getCondition()) {
		case EQUALS:
			if(condition.getValue().equals(fieldValue)) {
				return true;
			}
			break;
		case NOT_EQUALS:
			if(!condition.getValue().equals(fieldValue)) {
				return true;
			}
			break;
		}
		return false;
	}

	@Override
	public String toString() {
		return "FilterCondition [getField()=" + getField()
				+ ", getCondition()=" + getCondition() + ", getValue()="
				+ getValue() + "]";
	}
}