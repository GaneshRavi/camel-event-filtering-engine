package com.filter;

import java.util.List;

public class Scenario {
    public void setId(long id) {
		this.id = id;
	}
	public void setFilterChildren(List<Filter> filterChildren) {
		this.filterChildren = filterChildren;
	}
	private long id;
	private long vpId;
    private List<Filter> filterChildren;
    
	@Override
	public String toString() {
		return "Scenario [getId()=" + getId() + ", getFilterChildren()="
				+ getFilterChildren() + "]";
	}
	
	public long getId() {
		return id;
	}
	public List<Filter> getFilterChildren() {
		return filterChildren;
	}
	public long getVpId() {
		return vpId;
	}
	public void setVpId(long vp_id) {
		this.vpId = vp_id;
	}
}