package com.filter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A helper class for deploying a VP
 * 
 * @author Ganesh Ravi
 *
 */
public class FilterFileReaderHelper {

	private static Logger logger = LoggerFactory.getLogger(FilterFileReaderHelper.class);	
	
	private static final String FILTER_FILE_EXTENSION = "json";
	
	/**
	 * Finds all the files with the specified extension inside the given
	 * directory
	 * 
	 * @param directory The directory which has to be searched
	 * @param fileExtension The extension of the file to be searched for
	 * 
	 * @return Array of all matched file
	 */
	private static File[] getAllFilesInDirectory(
			File directory, final String fileExtension) {
		File [] files = directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(fileExtension);
			}
		});
		return files;
	}
	
	/**
	 * Returns all the SFM text present in all json files in the
	 * given directory
	 * 
	 * @param dirPath The directory which has to be searched
	 * 
	 * @return List of string representing the SFM JSON
	 */
	public static List<String> getAllSFMJsonFromDirectory(
			String dirPath) {
		List<String> sfmJsons = new ArrayList<String>();
		File dir = new File(dirPath);
		if(dir.exists()) {
			File [] files = getAllFilesInDirectory(dir, FILTER_FILE_EXTENSION);
			for(File file : files) {
				try {
					String sfmJson = IOUtils.toString(new FileReader(file.getAbsolutePath()));
					sfmJsons.add(sfmJson);
				} catch (FileNotFoundException e) {
					logger.error("Filter file not found: " + file.getAbsolutePath(), e);
				} catch (IOException e) {
					logger.error("IO Exception when trying to read file: " + file.getAbsolutePath(), e);
				}
			}
		}
		logger.info("Number of filter files read in directory " + dirPath + ": " + sfmJsons.size());
		return sfmJsons;
	}

}