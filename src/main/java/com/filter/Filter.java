package com.filter;

import java.util.List;

public class Filter {
    public void setId(long id) {
		this.id = id;
	}
	public void setConditions(List<FilterCondition> conditions) {
		this.conditions = conditions;
	}
	public void setFilterChildren(List<Filter> filterChildren) {
		this.filterChildren = filterChildren;
	}
	public long getId() {
		return id;
	}
	public List<FilterCondition> getConditions() {
		return conditions;
	}
	public List<Filter> getFilterChildren() {
		return filterChildren;
	}
	private long id;
    private List<FilterCondition> conditions;
    private List<Filter> filterChildren;
    
	@Override
	public String toString() {
		return "Filter [getId()=" + getId() + ", getConditions()="
				+ getConditions() + ", getFilterChildren()="
				+ getFilterChildren() + "]";
	}
}