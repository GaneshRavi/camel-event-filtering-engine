package com.filter;


/**
 * A tree with the Scenario node as its root node and
 * filter/mapping nodes as children. There will be one
 * such tree per Scenario
 * 
 * @author Ganesh Ravi
 *
 */
public class FiltMapTree {
	
    /**
     *  The root node of the tree
     */
    private Scenario rootScenario;

    public void setRootScenario(Scenario rootScenario) {
    	this.rootScenario = rootScenario;
    }
    
    public Scenario getRootScenario() {
    	return rootScenario;
    }

	@Override
	public String toString() {
		return "FiltMapTree [getRootScenario()=" + getRootScenario() + "]";
	}
    
}