package com.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

/**
 * 
 * A Parser class to parse the filter/mapping json file and
 * create a memory representation of it.
 * 
 * @author Ganesh Ravi
 *
 */
public class SFMJsonParser {

	private static final String FILTER_FIELD = "filter";
	private static final String FILTER_ID_FIELD = "filter_id";
	private static final String VP_ID_FIELD = "vp_id";
	private static final String SCENARIO_FIELD = "scenario";
	private static final String _ID_FIELD = "_id";

	private static Logger logger = LoggerFactory.getLogger(SFMJsonParser.class);

	public static List<FiltMapTree> read(List<String> jsons) throws JsonParseException,
	IOException {
		List<FiltMapTree> filtMapTrees = new ArrayList<FiltMapTree>();
		JsonFactory f = new JsonFactory();
		for(String json : jsons) {
			JsonParser jp = f.createParser(json);
			FiltMapTree filtMapTree = new FiltMapTree();
			if (jp.nextToken() != JsonToken.START_OBJECT) {
				throw new IllegalStateException("Start token not found");
			}
			Scenario scenario = null;
			List<Filter> scenarioFilterList = null;
			while (jp.nextToken() != JsonToken.END_OBJECT) {
				String fieldname = jp.getCurrentName();
				jp.nextToken();
				if (_ID_FIELD.equals(fieldname)) {
					scenario = new Scenario();
					scenario.setId(jp.getLongValue());
				} else if (VP_ID_FIELD.equals(fieldname)) {
					scenario.setVpId(jp.getLongValue());
				} else if (SCENARIO_FIELD.equals(fieldname)) {
					scenarioFilterList = parseFilterObjects(jp);
				}
			}
			jp.close();
			if (scenario != null && scenarioFilterList != null) {
				scenario.setFilterChildren(scenarioFilterList);
				filtMapTree.setRootScenario(scenario);
				filtMapTrees.add(filtMapTree);
			}
		}
		return filtMapTrees;
	}

	private static List<Filter> parseFilterObjects(JsonParser jp)
			throws IOException, JsonParseException {
		List<Filter> filterList = new ArrayList<>();
		while (jp.nextToken() != JsonToken.END_ARRAY) {
			Filter filter = new Filter();
			List<Filter> filterChildren = null;
			List<FilterCondition> conditionChildren = null;
			while (jp.nextToken() != JsonToken.END_OBJECT) {
				jp.nextToken();
				String filterField = jp.getCurrentName();
				if (FILTER_ID_FIELD.equals(filterField)) {
					filter.setId(jp.getLongValue());
				} else if (FILTER_FIELD.equals(filterField)) {
					if (filterChildren == null) {
						filterChildren = new ArrayList<Filter>();
					}
					filterChildren = parseFilterObjects(jp);
				} else {
					String conditionField = filterField, conditionOperator = null, conditionValue = null;
					while (jp.nextToken() != JsonToken.END_OBJECT) {
						conditionOperator = jp.getCurrentName();
						jp.nextToken();
						conditionValue = jp.getText();
					}
					FilterCondition filterCondition = new FilterCondition(
							conditionField,
							ConditionType.getEnum(conditionOperator),
							conditionValue);
					if (conditionChildren == null) {
						conditionChildren = new ArrayList<>();
					}
					logger.debug("Filter condition: " + filterCondition);
					conditionChildren.add(filterCondition);
				}
			}
			if (filter.getId() > 0 && conditionChildren != null) {
				filter.setConditions(conditionChildren);
				if (filterChildren != null) {
					filter.setFilterChildren(filterChildren);
				}
				logger.debug("Creating filter: " + filter);
				filterList.add(filter);
			}
		}
		return (filterList.isEmpty() ? null : filterList);
	}

}