package com.filter;

/**
 * This enum represents the possible conditional operators
 * allowed in a Filter.
 * 
 * @author Ganesh Ravi
 *
 */
public enum ConditionType {
	EQUALS("equals"), NOT_EQUALS("notEquals");
	
	private String value;
	
	private ConditionType(String value) {
		this.value = value;
	}
	
	public static ConditionType getEnum(String condition) {
		if(condition.equals(EQUALS.value)) {
			return EQUALS;
		} else {
			return NOT_EQUALS;
		}
	}
	
}