package com.objects;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * This class defines a generic Event. An Event is defined as a set of key
 * fields. The only meaningful key fields are those defined in the
 * filterfield.properties bundle file plus other supporting fields populated
 * during the filtering and mapping stages. Other properties bundle files
 * defines the details associated with each key field.
 */
public class Event {

	/**
	 * Holds the (variable number of) event fields.
	 */
	private Map<String, Object> fields = new HashMap<String, Object>();

	/**
	 * Returns the value of the event field referenced by the key.
	 * 
	 * @param key
	 *            The unique key used to reference the event field.
	 * @return The value of the field or an empty string if no key is found.
	 */
	public Object getField(String key) {
		String ret = "";
		String val = (String) fields.get(key);

		if (val == null && key != null & key instanceof String) {
			// try look-up in lower case too
			val = (String) fields.get(key.toString().toLowerCase());
		}

		if (val != null) {
			ret = val;
		}

		return ret;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getFields() {
		return fields;
	}

	/**
	 * Sets the value of the event field referenced by the key.
	 * 
	 * @param key
	 *            The unique key used to reference the event field.
	 * @param value
	 *            The field value to be set.
	 */
	@JsonAnySetter
	public void setField(String key, Object value) {
		fields.put(key, value);
	}

	/**
	 * Clears all fields associated with this Event.
	 */
	public final void clearFields() {
		fields.clear();
	}

	/**
	 * toString method
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Event [");

		int count = 0;
		for (Map.Entry<String, Object> entry : fields.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (count != 0) {
				sb.append(",");
			}
			String objectString = (null != value) ? value.toString() : "null";
			sb.append(" (key=" + key + " & value=" + objectString + ")");
			count++;
		}

		sb.append("]");

		return sb.toString();
	}
}