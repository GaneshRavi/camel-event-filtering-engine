package com.camel;

import org.apache.log4j.Logger;
import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class JsonEventCodec implements ProtocolCodecFactory {

	private static Logger logger = Logger.getLogger(JsonEventCodec.class);
	
	public ProtocolEncoder getEncoder() throws Exception {
		return new JsonEventEncoder();
	}
	public ProtocolDecoder getDecoder() throws Exception {
		return new JsonEventDecoder();
	}

	
	public static class JsonEventDecoder extends CumulativeProtocolDecoder {
		
		/**
		 * array of event end tag characters used for determining the end of an
		 * event.
		 */
		private static final char[] EVENT_END_TAG = { '}' };
		
		private static final String EVENT_START_TAG = "{";
		
		/**
		 * The name of the attribute that will be set on the session saying if
		 * this is the first read for the session
		 */
		private static final String ATTR_FIRST_READ_DONE = "FIRST_READ_DONE";
		
		/**
		 * The name of the attribute that will be set on the session containing the
		 * in complete event's text that has been read from the TCP buffer in the 
		 * previous run.
		 */
		private static final String ATTR_TEXT_READ = "TEXT_READ";
		
		@Override
		protected boolean doDecode(IoSession session,
				ByteBuffer in, ProtocolDecoderOutput out) throws Exception {
			
			if (in.remaining() >= EVENT_END_TAG.length) {
				int offset = 0;
				StringBuilder sb = new StringBuilder();
				// Get the part of the event previously read from the TCP buffer
				Object textRead = session.getAttribute(ATTR_TEXT_READ);
				if(textRead != null) {
					sb.append(textRead.toString());
				}
				int numberOfBytesToRead = in.remaining();
				for(int i = 0; i < numberOfBytesToRead; i++) {
					char c = (char)in.get();
					sb.append(c);
					if(c == EVENT_END_TAG[offset]) {
						if(offset == EVENT_END_TAG.length-1) {
							String event = sb.toString();
							if(session.getAttribute(ATTR_FIRST_READ_DONE) == null) {
								int indexOfStartEvent = event.indexOf(EVENT_START_TAG);
								if(indexOfStartEvent > -1) {
									event = event.substring(indexOfStartEvent);
								}
								session.setAttribute(ATTR_FIRST_READ_DONE);
							}
							out.write(event);
							session.removeAttribute(ATTR_TEXT_READ);
							logger.info("JSON Event decoder returning event: " + event);
							return true;
						}
						offset++;
					} else {
						offset = 0;
					}
				}
				session.setAttribute(ATTR_TEXT_READ, sb.toString());
			}
			return false;
		}
	}
	
	public class JsonEventEncoder implements ProtocolEncoder {
		@Override
		public void dispose(IoSession arg0) throws Exception {
		}

		@Override
		public void encode(IoSession arg0, Object arg1, ProtocolEncoderOutput arg2)
				throws Exception {
		}
	}
	
}