/**
 * 
 */
package com.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filter.FilteringEngine;
import com.objects.Event;

import db.access.DBEngine;
import db.access.DBEngineIF;

public class StateWiseCamelFMProcessor implements Processor {
	
	private static Logger logger = LoggerFactory.getLogger(StateWiseCamelFMProcessor.class);
	private static DBEngineIF dbEngine = new DBEngine().getInstance();
	
	private FilteringEngine fmEngine;
	
	public StateWiseCamelFMProcessor(String filterDirPath) {
		fmEngine = new FilteringEngine(filterDirPath);
	}

	public void process(Exchange exchange) throws Exception {
		Event event = exchange.getIn().getBody(Event.class);
		boolean passedFilters = fmEngine.applyFilters(event);
		if(passedFilters) {
			logger.info("Event passed the filters: " + event);
			dbEngine.addEvent(event);
		} else {
			logger.info("Event did not pass the filters: " + event);
		}
		exchange.getOut().setBody(event);
	}
	
}