package db.access;

public class DBEngine {

	private static DBEngineIF INSTANCE;
	
	public DBEngine() {
		if(INSTANCE == null) {
			INSTANCE = new DbEngineMongo(new ConnectionDetails());
		}
	}
	
	public DBEngine(ConnectionDetails connectionDetails) {
		if(INSTANCE == null) {
			INSTANCE = new DbEngineMongo(connectionDetails);
		}
	}
	
	public DBEngineIF getInstance() {
		return INSTANCE;
	}
	
}
