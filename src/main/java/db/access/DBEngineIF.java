package db.access;

import com.objects.Event;


public interface DBEngineIF {
	
	public int addEvent(Event event);
	
	public Event getEvent(int eventId);

	void clearAllEvents();
	
}