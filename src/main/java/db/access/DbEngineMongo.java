package db.access;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.objects.Event;

public class DbEngineMongo implements DBEngineIF{
	
	private static Logger logger = LoggerFactory.getLogger(DbEngineMongo.class);
	private static final String EVENTS_COLLECTION_NAME = "events";
	private DB db;
	private MongoClient mongoClient;
	private int eventsSequenceId;
	
	public DbEngineMongo(ConnectionDetails connDetails) {
		try {
			mongoClient = new MongoClient(connDetails.getDbHost(), connDetails.getDbPort());
			db = mongoClient.getDB(connDetails.getDbName());
			logger.info("Database Connection established to: " + connDetails);
			initialiseSequenceIds();
		} catch (UnknownHostException e) {
			logger.error("Unknown DB host specified", e);
		}
	}
	
	private void initialiseSequenceIds() {
		DBCollection eventsCollection = db.getCollection(EVENTS_COLLECTION_NAME);
		DBCursor eventsCursor = eventsCollection.find().sort(new BasicDBObject().append("event_id", -1)).limit(1);
		if(eventsCursor.hasNext()) {
			DBObject dbObject = eventsCursor.next();
			Integer eventssSequenceIdInteger = (Integer)dbObject.get("event_id");
			if(eventssSequenceIdInteger != null && eventssSequenceIdInteger.intValue() > 0) {
				eventsSequenceId = eventssSequenceIdInteger.intValue();
			}
		}
	}
	
	@Override
	public void clearAllEvents() {
		DBCollection eventsCollection = db.getCollection(EVENTS_COLLECTION_NAME);
		eventsCollection.drop();
		eventsSequenceId = 0;
	}

	@Override
	public int addEvent(Event event) {
		DBCollection eventsCollection = db.getCollection(EVENTS_COLLECTION_NAME);
		ObjectWriter ow = new ObjectMapper().writer();
		String json;
		try {
			json = ow.writeValueAsString(event);
			BasicDBObject dbObject = (BasicDBObject) JSON.parse(json);
			dbObject.append("event_id", ++eventsSequenceId);
			eventsCollection.save(dbObject);
			eventsCollection.save(new BasicDBObject());
			return eventsSequenceId;
		} catch (JsonProcessingException e) {
			logger.error("Error when converting event to json string: ", e);
		}
		return -1;
	}

	@Override
	public Event getEvent(int eventId) {
		return null;
	}	
	
}