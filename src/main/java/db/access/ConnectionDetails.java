package db.access;

import org.apache.log4j.Logger;

import com.configuration.EventFilterPropertiesHelper;


/**
 * database connection details
 */
public class ConnectionDetails {

	public static final String DATABASE_DBMS_PROPERTY = "database.dbms";

	public static final String DB_PASSWORD_PROPERTY = "db.password";

	public static final String DB_USERNAME_PROPERTY = "db.username";

	public static final String DB_HOST_PROPERTY = "db.host";
	
	public static final String DB_NAME_PROPERTY = "db.name";

	public static final String DB_PORT_PROPERTY = "db.port";

	/** the log4j logger */
	private static Logger log = Logger.getLogger(ConnectionDetails.class);

	/** the database name */
	private String dbName;

	/** Used for storing the database username. */
	private String dbUsername;

	/** Used for storing the database password. */
	private String dbPassword;

	/** the database server hostname */
	private String dbHostName;

	/** the database server port */
	private String dbPortText;

	/**
	 * 
	 * Default constructor using values from statewise.properties
	 * 
	 * @param name
	 */
	public ConnectionDetails() {
		dbName = EventFilterPropertiesHelper.getProperty(DB_NAME_PROPERTY);
		dbHostName = EventFilterPropertiesHelper.getProperty(DB_HOST_PROPERTY);
		dbPortText = EventFilterPropertiesHelper.getProperty(DB_PORT_PROPERTY);
		dbUsername = EventFilterPropertiesHelper.getProperty(DB_USERNAME_PROPERTY);
		dbPassword = EventFilterPropertiesHelper.getProperty(DB_PASSWORD_PROPERTY);

		if (!isValid()) {
			throw new IllegalArgumentException("Database connection details are not valid.");
		}
	}

	/**
	 * 
	 * Default constructor.
	 * 
	 * @param dbName
	 * @param connectionURL
	 * @param dbUsername
	 * @param dbPassword
	 * @param jdbcDriver
	 * @param dbType
	 * @param maxConnections
	 */
	public ConnectionDetails(String dbName, String dbHost, String dbPort, String dbUsername, String dbPassword) {
		this.dbName = dbName;
		this.dbHostName = dbHost;
		this.dbPortText = dbPort;
		this.dbUsername = dbUsername;
		this.dbPassword = dbPassword;

		if (!isValid()) {
			throw new IllegalArgumentException("Database connection details are not valid.");
		}
	}

	/**
	 * Getter for the DB username
	 * 
	 * @return String
	 */
	public String getDbUsername() {
		return dbUsername;
	}

	/**
	 * Setter for the DB username
	 * 
	 * @param dbUsername
	 */
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	/**
	 * Getter for the DB password
	 * 
	 * @return String
	 */
	public String getDbPassword() {
		return dbPassword;
	}

	/**
	 * Setter for the DB password
	 * 
	 * @param dbPassword
	 */
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	/**
	 * Getter for the DB name
	 * 
	 * @return String
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * Setter for the DB name
	 * 
	 * @param dbName
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * 
	 * @return String
	 */
	public String getDbHost() {
		return dbHostName;
	}

	/**
	 * 
	 * @return String
	 */
	public int getDbPort() {
		return Integer.parseInt(dbPortText);
	}

	/**
	 * 
	 * @param dbHostname
	 */
	public void setDbHost(String dbHostname) {
		this.dbHostName = dbHostname;
	}

	/**
	 * 
	 * @param dbPort
	 */
	public void setDbPort(String dbPort) {
		this.dbPortText = dbPort;
	}

	/**
	 * return whether the details are valid
	 * 
	 * @return boolean
	 */
	public boolean isValid() {
		if(dbName == null) {
			log.error("Database name is null");
		}
		if(dbUsername == null) {
			log.error("Database username is null");
		}
		if(dbPassword == null) {
			log.error("Database password is null");
		}
		if(dbHostName == null) {
			log.error("Database hostname is null");
		}
		boolean isPortValid = false;
		if(dbPortText == null) {
			log.error("Database port is null");
		} else {
			try {
				Integer.parseInt(dbPortText);
				isPortValid = true;
			} catch(NumberFormatException e) {
				log.error("Database port is not defined correctly"); 
			}
		}
		return (dbName != null && dbUsername != null && dbPassword != null && dbHostName != null && isPortValid);
	}

	/**
	 * Convert to a string
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("dbName = " + dbName + "\n");
		builder.append("dbUsername = " + dbUsername + "\n");
		builder.append("dbPassword = " + dbPassword + "\n");
		builder.append("dbHost = " + dbHostName + "\n");
		builder.append("dbPort = " + dbPortText);
		return builder.toString();
	}
}
